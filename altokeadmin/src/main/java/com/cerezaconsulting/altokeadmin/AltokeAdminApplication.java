package com.cerezaconsulting.altokeadmin;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by miguel on 24/02/17.
 */

public class AltokeAdminApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/gotham_light.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
