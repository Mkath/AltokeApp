package com.cerezaconsulting.altokeadmin.presentation.activity;

import android.os.Bundle;

import com.cerezaconsulting.altokeadmin.R;
import com.cerezaconsulting.altokeadmin.core.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
