package com.cerezaconsulting.altoke.data.repositories.local;

import android.content.Context;
import android.content.SharedPreferences;

import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.entities.UserEntity;
import com.google.gson.Gson;

/**
 * Created by miguel on 22/02/17.
 */

public class SessionManager {
    private static final String PREFERENCE_NAME = "ckeck_in";
    private static int PRIVATE_MODE = 0;

    /**
     USER DATA SESSION - JSON
     */

    private static final String USER_JSON = "user";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String PLACE_CHOICE = "place_choice";
    private static final String IS_LOGIN = "is_login";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREFERENCE_NAME,PRIVATE_MODE);
        editor = preferences.edit();
    }

    public void openSession(String token, UserEntity userEntity){
        editor.putString(ACCESS_TOKEN,token);
        editor.putBoolean(IS_LOGIN,true);
        String user = new Gson().toJson(userEntity);
        editor.putString(USER_JSON,user);
        editor.commit();
    }

    public void selectPlace(PlacesEntity placesEntity){
        String place = new Gson().toJson(placesEntity);
        editor.putString(PLACE_CHOICE,place);
        editor.commit();
    }

    public String getUserToken(){
        if (preferences.getBoolean(IS_LOGIN, false)) {
            return preferences.getString(ACCESS_TOKEN,null);
        }
        return null;
    }

    public PlacesEntity getPlace(){
        PlacesEntity placesEntity = null;
        String place = preferences.getString(PLACE_CHOICE,null);
        if(place!=null){
            placesEntity = new Gson().fromJson(place,PlacesEntity.class);
        }
        return placesEntity;
    }

    public UserEntity getUserEntity(){
        UserEntity userEntity = null;
        String user = preferences.getString(USER_JSON,null);
        if(user!=null){
            userEntity = new Gson().fromJson(user,UserEntity.class);
        }
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity){
        String user = new Gson().toJson(userEntity);
        editor.putString(USER_JSON,user);
        editor.commit();
    }

    public void regID(String regID) {

        editor.putString(PROPERTY_REG_ID, regID);
        editor.commit();
    }

    public boolean isLogin(){
        return preferences.getBoolean(IS_LOGIN,false);
    }

    public void closeSession(){
        editor.putString(ACCESS_TOKEN,null);
        editor.putBoolean(IS_LOGIN,false);
        editor.putString(USER_JSON,null);
        editor.commit();
    }
}
