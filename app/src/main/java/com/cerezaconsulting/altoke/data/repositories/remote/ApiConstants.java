package com.cerezaconsulting.altoke.data.repositories.remote;

/**
 * URL BASE
 */
public class ApiConstants {

    //CONSTANTS ACTIVITY FOR RESULT
    public static final int SEND_REQUEST = 123;
    public static final int MENU_RESULT = 2903;

    //Auth
    public static final String USER_LOGIN_FACEBOOK = "mobile/facebook/";
    public static final String USER_LOGIN_GMAIL = "mobile/google-oauth2/";
    public static final String USER_RETRIEVE = "user/retrieve/";

    //Places
    public static final String LIST_PLACES = "listplaces/";

    //Food by place
    public static final String FOOD_BY_PLACE = "listlunchsplaceanddays/{pk}/";

    //Reservations
    public static final String RESERVATIONS = "create/reservation/";
    public static final String GET_MY_RESERVATIONS = "me/reservations/";

    public static final String SEND_FIREBASE = "me/devices/fcm/";
    public static final String UPDATE_FIREBASE = "me/devices/fcm/update/";

}
