package com.cerezaconsulting.altoke.data.repositories.remote.request;

import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.entities.trackHolders.TrackEntityHolder;
import com.cerezaconsulting.altoke.data.repositories.remote.ApiConstants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by miguel on 10/04/17.
 */

public interface PlacesRequest {

    @GET(ApiConstants.LIST_PLACES)
    Call<TrackEntityHolder<PlacesEntity>> getPlaces(@Query("page") int page);

}
