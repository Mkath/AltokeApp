package com.cerezaconsulting.altoke.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 17/04/17.
 */

public class FoodEntity implements Serializable {
    private String id;
    private String main_dish;
    private String entry;
    private String name_lunch;
    private String drink;
    private int stock;
    private String day;
    private boolean is_enabled;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMain_dish() {
        return main_dish;
    }

    public void setMain_dish(String main_dish) {
        this.main_dish = main_dish;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public boolean is_enabled() {
        return is_enabled;
    }

    public void setIs_enabled(boolean is_enabled) {
        this.is_enabled = is_enabled;
    }

    public String getName_lunch() {
        return name_lunch;
    }

    public void setName_lunch(String name_lunch) {
        this.name_lunch = name_lunch;
    }
}
