package com.cerezaconsulting.altoke.data.repositories.remote.request;

import com.cerezaconsulting.altoke.data.entities.AccessTokenEntity;
import com.cerezaconsulting.altoke.data.repositories.remote.ApiConstants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by miguel on 10/04/17.
 */

public interface LoginRequest {

    @FormUrlEncoded
    @POST(ApiConstants.USER_LOGIN_FACEBOOK)
    Call<AccessTokenEntity> loginFacebook(@Field("access_token") String tokenFace);

    @FormUrlEncoded
    @POST(ApiConstants.USER_LOGIN_GMAIL)
    Call<AccessTokenEntity> loginGmail(@Field("access_token") String access_token);

}
