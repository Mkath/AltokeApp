package com.cerezaconsulting.altoke.data.entities.enums;

/**
 * Created by miguel on 17/04/17.
 */

public enum DaysEnum {
    LUNES,
    MARTES,
    MIERCOLES,
    JUEVES,
    VIERNES,
    SABADO,
    DOMINGO
}
