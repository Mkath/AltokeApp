package com.cerezaconsulting.altoke.data.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by miguel on 20/04/17.
 */

public class DateEntity implements Serializable {
    private String day;
    private Date date;

    public DateEntity(String day, Date date) {
        this.day = day;
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
