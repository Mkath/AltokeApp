package com.cerezaconsulting.altoke.data.repositories.remote.request;

import com.cerezaconsulting.altoke.data.entities.FoodEntity;
import com.cerezaconsulting.altoke.data.entities.trackHolders.TrackEntityHolder;
import com.cerezaconsulting.altoke.data.repositories.remote.ApiConstants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by miguel on 17/04/17.
 */

public interface FoodRequest {
    @GET(ApiConstants.FOOD_BY_PLACE)
    Call<TrackEntityHolder<FoodEntity>> getFoodByPlace(@Path("pk") String placeId, @Query("search") String day);

}
