package com.cerezaconsulting.altoke.data.entities;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by miguel on 20/04/17.
 */

public class FoodByDayEntity implements Serializable {
    private String day;
    private Date date;
    private ArrayList<FoodEntity> foodEntities;

    public FoodByDayEntity(String day) {
        this.day = day;
        this.foodEntities = new ArrayList<>();
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public ArrayList<FoodEntity> getFoodEntities() {
        return foodEntities;
    }

    public void setFoodEntities(ArrayList<FoodEntity> foodEntities) {
        this.foodEntities = foodEntities;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
