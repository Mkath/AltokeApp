package com.cerezaconsulting.altoke.data.repositories.remote.request;

import com.cerezaconsulting.altoke.data.entities.ReservationEntity;
import com.cerezaconsulting.altoke.data.entities.UserEntity;
import com.cerezaconsulting.altoke.data.entities.trackHolders.TrackEntityHolder;
import com.cerezaconsulting.altoke.data.repositories.remote.ApiConstants;
import com.google.android.gms.common.api.Api;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by miguel on 10/04/17.
 */

public interface UserRequest {
    @GET(ApiConstants.USER_RETRIEVE)
    Call<UserEntity> getUser(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(ApiConstants.RESERVATIONS)
    Call<ReservationEntity> createReservation(@Header("Authorization") String token, @Field("lunch") String idFood,
                                              @Field("place_desired") String idPlace, @Field("day_reservation") String date,
                                              @Field("quantity") String quantity,@Field("is_enabled") boolean enabled);

    @GET(ApiConstants.GET_MY_RESERVATIONS)
    Call<TrackEntityHolder<ReservationEntity>> getMyReservations(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(ApiConstants.SEND_FIREBASE)
    Call<Void> sendFCM(@Header("Authorization") String token, @Field("registration_id") String id,
                       @Field("device_id") String device, @Field("type") String tipo);

    @FormUrlEncoded
    @PUT(ApiConstants.UPDATE_FIREBASE)
    Call<Void> updateFCM(@Header("Authorization") String token, @Field("registration_id") String id,
                         @Field("device_id") String device);

}
