package com.cerezaconsulting.altoke.presentation.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.data.entities.FoodByDayEntity;
import com.cerezaconsulting.altoke.data.entities.FoodEntity;
import com.cerezaconsulting.altoke.presentation.contracts.MenuWeekContract;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 20/04/17.
 */

public class MenuWeekAdapter extends RecyclerView.Adapter<MenuWeekAdapter.ViewHolder> {

    private ArrayList<FoodByDayEntity> list;
    private MenuWeekContract.Presenter mView;
    private ItemWeekAdapter adapter;
    private Context context;
    private int pos = 0;

    public MenuWeekAdapter(ArrayList<FoodByDayEntity> list, MenuWeekContract.Presenter mView, Context context) {
        this.list = list;
        this.mView = mView;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_week_list, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ArrayList<FoodEntity> arrayList = list.get(position).getFoodEntities();
        this.adapter = new ItemWeekAdapter(arrayList, context, list.get(position).getDate(), mView);
        holder.rvSelectMenu.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.rvSelectMenu.setAdapter(adapter);
        if (list.get(position).getFoodEntities().size() != 0) {
            holder.tvNoMenu.setVisibility(View.GONE);
        }
        showArrow(holder.ivArrowLeft,holder.ivArrowRight,pos,arrayList.size());
        holder.ivArrowLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos--;
                holder.rvSelectMenu.scrollToPosition(pos);
                showArrow(holder.ivArrowLeft,holder.ivArrowRight,pos,arrayList.size());
            }
        });
        holder.ivArrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos++;
                holder.rvSelectMenu.scrollToPosition(pos);
                showArrow(holder.ivArrowLeft,holder.ivArrowRight,pos,arrayList.size());
            }
        });
    }

    public void setPos(int pos){
        this.pos = pos;
        notifyDataSetChanged();
    }

    private void showArrow(ImageView ivArrowLeft, ImageView ivArrowRight,int position, int size){
        ivArrowLeft.setVisibility((position!=0 && size>1)?View.VISIBLE:View.GONE);
        ivArrowRight.setVisibility((position!=size-1 && size>1)?View.VISIBLE:View.GONE);
    }

    public void setList(ArrayList<FoodByDayEntity> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rv_select_menu)
        RecyclerView rvSelectMenu;
        @BindView(R.id.iv_arrow_right)
        ImageView ivArrowRight;
        @BindView(R.id.iv_arrow_left)
        ImageView ivArrowLeft;
        @BindView(R.id.tv_no_menu)
        TextView tvNoMenu;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
