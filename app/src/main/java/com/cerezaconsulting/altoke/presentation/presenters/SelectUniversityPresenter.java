package com.cerezaconsulting.altoke.presentation.presenters;

import android.content.Context;

import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.entities.trackHolders.TrackEntityHolder;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ServiceFactory;
import com.cerezaconsulting.altoke.data.repositories.remote.request.PlacesRequest;
import com.cerezaconsulting.altoke.presentation.contracts.SelectUniversityContract;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 10/04/17.
 */

public class SelectUniversityPresenter implements SelectUniversityContract.Presenter {

    private SelectUniversityContract.View mView;
    private SessionManager sessionManager;
    private int page=0;

    public SelectUniversityPresenter(SelectUniversityContract.View mView, Context context) {
        this.mView = mView;
        sessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
        page = 1;
        loadPlaces();
    }

    private void loadPlaces(final int pages){
        mView.setLoadingIndicator(true);
        PlacesRequest placesRequest = ServiceFactory.createService(PlacesRequest.class);
        Call<TrackEntityHolder<PlacesEntity>> call = placesRequest.getPlaces(pages);
        call.enqueue(new Callback<TrackEntityHolder<PlacesEntity>>() {
            @Override
            public void onResponse(Call<TrackEntityHolder<PlacesEntity>> call, Response<TrackEntityHolder<PlacesEntity>> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    if(response.body().getNext()!=null) {
                        page++;
                    }
                    else{
                        page=0;
                    }
                    mView.showPlaces(response.body().getResults());

                }
                else{
                    mView.setMessageError("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<TrackEntityHolder<PlacesEntity>> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.setMessageError("No se pudo conectar al servidor, por favor intente más tarde");
            }
        });
    }

    @Override
    public void loadPlaces() {
        loadPlaces(page);
    }

    @Override
    public void loadMorePlaces() {
        if(page!=0){
            loadPlaces();
        }
    }
}
