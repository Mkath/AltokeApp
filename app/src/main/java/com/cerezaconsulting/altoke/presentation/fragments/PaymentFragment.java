package com.cerezaconsulting.altoke.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseFragment;

/**
 * Created by miguel on 9/05/17.
 */

public class PaymentFragment extends BaseFragment {

    public static PaymentFragment newInstance(){
        return new PaymentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_payment,container,false);
        return root;
    }
}
