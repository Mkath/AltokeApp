package com.cerezaconsulting.altoke.presentation.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.core.BaseFragment;
import com.cerezaconsulting.altoke.data.entities.DateEntity;
import com.cerezaconsulting.altoke.data.entities.FoodByDayEntity;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ApiConstants;
import com.cerezaconsulting.altoke.presentation.activities.SelectUniversityActivity;
import com.cerezaconsulting.altoke.presentation.adapters.MenuWeekAdapter;
import com.cerezaconsulting.altoke.presentation.contracts.MenuWeekContract;
import com.cerezaconsulting.altoke.presentation.utils.DayUtils;
import com.cerezaconsulting.altoke.presentation.utils.ProgressDialogCustom;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miguel on 16/02/17.
 */

public class MenuWeekFragment extends BaseFragment implements MenuWeekContract.View {


    @BindView(R.id.tv_monday_date)
    TextView tvMondayDate;
    @BindView(R.id.ll_monday)
    LinearLayout llMonday;
    @BindView(R.id.tv_tuesday_date)
    TextView tvTuesdayDate;
    @BindView(R.id.ll_tuesday)
    LinearLayout llTuesday;
    @BindView(R.id.tv_wednesday_date)
    TextView tvWednesdayDate;
    @BindView(R.id.ll_wednesday)
    LinearLayout llWednesday;
    @BindView(R.id.tv_thursday_date)
    TextView tvThursdayDate;
    @BindView(R.id.ll_thursday)
    LinearLayout llThursday;
    @BindView(R.id.tv_friday_date)
    TextView tvFridayDate;
    @BindView(R.id.ll_friday)
    LinearLayout llFriday;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.ll_select_delivery_point)
    LinearLayout llSelectDeliveryPoint;
    @BindView(R.id.tv_delivery_point)
    TextView tvDeliveryPoint;
    @BindView(R.id.v_select_monday)
    View vSelectMonday;
    @BindView(R.id.v_select_tuesday)
    View vSelectTuesday;
    @BindView(R.id.v_select_wednesday)
    View vSelectWednesday;
    @BindView(R.id.v_select_thursday)
    View vSelectThursday;
    @BindView(R.id.v_select_friday)
    View vSelectFriday;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    private MenuWeekContract.Presenter presenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private MenuWeekAdapter adapter;
    private SessionManager sessionManager;

    public static MenuWeekFragment newInstance() {
        return new MenuWeekFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_menu_week, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getWeekDates();
        sessionManager = new SessionManager(getContext());
        PlacesEntity placesEntity = sessionManager.getPlace();
        tvDeliveryPoint.setText(placesEntity.getName());
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Ingresando...");
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvList.setLayoutManager(layoutManager);
        adapter = new MenuWeekAdapter(new ArrayList<FoodByDayEntity>(), presenter, getContext());
        rvList.setAdapter(adapter);
        rvList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        selectDay(R.id.ll_monday);
        /*SnapHelper helper = new LinearSnapHelper();
        helper.attachToRecyclerView(rvList);*/
    }

    public void refresh() {
        cleanView();
        presenter.start();
    }

    private void cleanView() {
        llTuesday.setBackground(getResources().getDrawable(R.drawable.circle_unselect));
        tvTuesdayDate.setTextColor(getResources().getColor(R.color.secondaryText));
        llMonday.setBackground(getResources().getDrawable(R.drawable.circle_unselect));
        tvMondayDate.setTextColor(getResources().getColor(R.color.secondaryText));
        llWednesday.setBackground(getResources().getDrawable(R.drawable.circle_unselect));
        tvWednesdayDate.setTextColor(getResources().getColor(R.color.secondaryText));
        llThursday.setBackground(getResources().getDrawable(R.drawable.circle_unselect));
        tvThursdayDate.setTextColor(getResources().getColor(R.color.secondaryText));
        llFriday.setBackground(getResources().getDrawable(R.drawable.circle_unselect));
        tvFridayDate.setTextColor(getResources().getColor(R.color.secondaryText));
    }

    private void getWeekDates() {
        ArrayList<DateEntity> list = DayUtils.getDates();
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < list.size(); i++) {
            switch (list.get(i).getDay()) {
                case "lunes":
                    calendar.setTime(list.get(i).getDate());
                    tvMondayDate.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                    break;
                case "martes":
                    calendar.setTime(list.get(i).getDate());
                    tvTuesdayDate.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                    break;
                case "miercoles":
                    calendar.setTime(list.get(i).getDate());
                    tvWednesdayDate.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                    break;
                case "jueves":
                    calendar.setTime(list.get(i).getDate());
                    tvThursdayDate.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                    break;
                case "viernes":
                    calendar.setTime(list.get(i).getDate());
                    tvFridayDate.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void loadDays(ArrayList<FoodByDayEntity> list) {
        if (adapter != null) {
            adapter.setList(list);
        }
    }

    @Override
    public void getTabPositionChange(int position) {
        switch (position) {
            case Calendar.TUESDAY:
                llTuesday.setBackground(getResources().getDrawable(R.drawable.circle_select));
                tvTuesdayDate.setTextColor(getResources().getColor(R.color.white));
                break;
            case Calendar.MONDAY:
                llMonday.setBackground(getResources().getDrawable(R.drawable.circle_select));
                tvMondayDate.setTextColor(getResources().getColor(R.color.white));
                break;
            case Calendar.WEDNESDAY:
                llWednesday.setBackground(getResources().getDrawable(R.drawable.circle_select));
                tvWednesdayDate.setTextColor(getResources().getColor(R.color.white));
                break;
            case Calendar.THURSDAY:
                llThursday.setBackground(getResources().getDrawable(R.drawable.circle_select));
                tvThursdayDate.setTextColor(getResources().getColor(R.color.white));
                break;
            case Calendar.FRIDAY:
                llFriday.setBackground(getResources().getDrawable(R.drawable.circle_select));
                tvFridayDate.setTextColor(getResources().getColor(R.color.white));
                break;
        }
    }

    @Override
    public void reserveFoodSuccessfully() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.reserve_successfully))
                .setPositiveButton(getString(R.string.accept), null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        refresh();
                    }
                })
                .create();
        alertDialog.show();
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        pbLoading.setVisibility(active?View.VISIBLE:View.GONE);
        // Make sure setRefreshing() is called after the layout is done with everything else.
    }

    @Override
    public void setMessageError(String error) {
        ((BaseActivity) getActivity()).showMessageError(error);
    }

    @Override
    public void setDialogMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(MenuWeekContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @OnClick({R.id.ll_monday, R.id.ll_tuesday, R.id.ll_wednesday, R.id.ll_thursday, R.id.ll_friday, R.id.ll_select_delivery_point})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_monday:
                rvList.scrollToPosition(0);
                adapter.setPos(0);
                selectDay(R.id.ll_monday);
                break;
            case R.id.ll_tuesday:
                rvList.scrollToPosition(1);
                selectDay(R.id.ll_tuesday);
                adapter.setPos(0);
                break;
            case R.id.ll_wednesday:
                rvList.scrollToPosition(2);
                selectDay(R.id.ll_wednesday);
                adapter.setPos(0);
                break;
            case R.id.ll_thursday:
                rvList.scrollToPosition(3);
                selectDay(R.id.ll_thursday);
                adapter.setPos(0);
                break;
            case R.id.ll_friday:
                rvList.scrollToPosition(4);
                selectDay(R.id.ll_friday);
                adapter.setPos(0);
                break;
            case R.id.ll_select_delivery_point:
                Intent i = new Intent(getContext(), SelectUniversityActivity.class);
                i.putExtra("activity_result", true);
                startActivityForResult(i, ApiConstants.SEND_REQUEST);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ApiConstants.SEND_REQUEST) {
            if (resultCode == ApiConstants.MENU_RESULT) {
                tvDeliveryPoint.setText(sessionManager.getPlace().getName());
            }
        }
    }

    private void selectDay(int id) {
        vSelectMonday.setVisibility((id == R.id.ll_monday) ? View.VISIBLE : View.GONE);
        vSelectTuesday.setVisibility((id == R.id.ll_tuesday) ? View.VISIBLE : View.GONE);
        vSelectWednesday.setVisibility((id == R.id.ll_wednesday) ? View.VISIBLE : View.GONE);
        vSelectThursday.setVisibility((id == R.id.ll_thursday) ? View.VISIBLE : View.GONE);
        vSelectFriday.setVisibility((id == R.id.ll_friday) ? View.VISIBLE : View.GONE);
    }

}
