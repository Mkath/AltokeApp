package com.cerezaconsulting.altoke.presentation.adapters.listeners;

/**
 * Created by miguel on 23/02/17.
 */

public interface OnClickListListener {
    void onClick(int position);
}
