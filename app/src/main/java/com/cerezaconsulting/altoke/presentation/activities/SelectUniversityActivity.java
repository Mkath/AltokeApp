package com.cerezaconsulting.altoke.presentation.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.data.repositories.remote.ApiConstants;
import com.cerezaconsulting.altoke.presentation.fragments.SelectUniversityFragment;
import com.cerezaconsulting.altoke.presentation.presenters.SelectUniversityPresenter;
import com.cerezaconsulting.altoke.presentation.utils.ActivityUtils;

/**
 * Created by miguel on 16/02/17.
 */

public class SelectUniversityActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clean);


        SelectUniversityFragment fragment = (SelectUniversityFragment) getSupportFragmentManager().findFragmentById(R.id.body);

        if(fragment==null){
            Bundle bundle = getIntent().getExtras();
            if(bundle==null){
                bundle = new Bundle();
            }
            fragment = SelectUniversityFragment.newInstance(bundle);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),fragment,R.id.body);
        }
        new SelectUniversityPresenter(fragment,getApplicationContext());
    }

    public void responseRequest(){
        setResult(ApiConstants.MENU_RESULT);
        finish();
    }
}
