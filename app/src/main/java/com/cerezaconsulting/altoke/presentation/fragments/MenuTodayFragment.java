package com.cerezaconsulting.altoke.presentation.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.core.BaseFragment;
import com.cerezaconsulting.altoke.core.ScrollChildSwipeRefreshLayout;
import com.cerezaconsulting.altoke.data.entities.FoodEntity;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ApiConstants;
import com.cerezaconsulting.altoke.presentation.activities.SelectUniversityActivity;
import com.cerezaconsulting.altoke.presentation.adapters.MenuTodayAdapter;
import com.cerezaconsulting.altoke.presentation.contracts.MenuTodayContract;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miguel on 16/02/17.
 */

public class MenuTodayFragment extends BaseFragment implements MenuTodayContract.View {


    @BindView(R.id.rv_select_menu)
    RecyclerView rvSelectMenu;
    @BindView(R.id.btn_reserve_combo)
    Button btnReserveCombo;
    @BindView(R.id.srl_menu)
    ScrollChildSwipeRefreshLayout srlMenu;
    @BindView(R.id.tv_select_point)
    TextView tvSelectPoint;
    @BindView(R.id.ll_select_delivery_point)
    LinearLayout llSelectDeliveryPoint;
    @BindView(R.id.iv_arrow_right)
    ImageView ivArrowRight;
    @BindView(R.id.iv_arrow_left)
    ImageView ivArrowLeft;

    private MenuTodayContract.Presenter presenter;
    private SessionManager sessionManager;
    private int position=0;
    private int size=0;
    private PlacesEntity placesEntity;
    private LinearLayoutManager layoutManager;
    private MenuTodayAdapter menuTodayAdapter;

    public static MenuTodayFragment newInstance() {
        return new MenuTodayFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_menu_today, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sessionManager = new SessionManager(getContext());
        placesEntity = sessionManager.getPlace();
        srlMenu.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.black),
                ContextCompat.getColor(getActivity(), R.color.dark_gray),
                ContextCompat.getColor(getActivity(), R.color.black)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        srlMenu.setScrollUpChild(rvSelectMenu);
        srlMenu.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.start();
            }
        });
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvSelectMenu.setLayoutManager(layoutManager);
        tvSelectPoint.setText(placesEntity.getName());
        menuTodayAdapter = new MenuTodayAdapter(new ArrayList<FoodEntity>(), getContext());
        rvSelectMenu.setAdapter(menuTodayAdapter);
        rvSelectMenu.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        /*SnapHelper helper = new LinearSnapHelper();
        helper.attachToRecyclerView(rvSelectMenu);*/
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    public void refresh(){
        presenter.start();
    }

    private void showQuantityDialog(final FoodEntity foodEntity) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_reserve_menu);
        Button button = (Button) dialog.findViewById(R.id.btn_accept);
        final EditText editText = (EditText) dialog.findViewById(R.id.et_quantity);
        ImageView close = (ImageView) dialog.findViewById(R.id.iv_close);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String quantity = editText.getText().toString();
                if (!quantity.isEmpty()) {
                    int q = Integer.parseInt(quantity);
                    if (q > 0 && q <= foodEntity.getStock()) {
                        presenter.createReservation(foodEntity.getId(), quantity);
                        dialog.dismiss();
                    } else {
                        setMessageError(getString(R.string.please_enter_a_valid_quantity));
                    }
                } else {
                    setMessageError(getString(R.string.please_enter_a_valid_quantity));
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void loadFood(ArrayList<FoodEntity> list) {
        if (menuTodayAdapter != null) {
            size=list.size();
            menuTodayAdapter.setList(list);
            showArrow();
        }
    }

    private void showArrow(){
        ivArrowLeft.setVisibility((position!=0 && size>1)?View.VISIBLE:View.GONE);
        ivArrowRight.setVisibility((position!=size-1 && size>1)?View.VISIBLE:View.GONE);
    }

    @Override
    public void reserveFoodSuccessfully() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.reserve_successfully))
                .setPositiveButton(getString(R.string.accept), null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        presenter.start();
                    }
                }).create();
        alertDialog.show();
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srlMenu.post(new Runnable() {
            @Override
            public void run() {
                srlMenu.setRefreshing(active);
            }
        });
    }

    @Override
    public void setMessageError(String error) {
        ((BaseActivity) getActivity()).showMessageError(error);
    }

    @Override
    public void setDialogMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(MenuTodayContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @OnClick({R.id.ll_select_delivery_point, R.id.btn_reserve_combo,R.id.iv_arrow_right,R.id.iv_arrow_left})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_select_delivery_point:
                Intent i = new Intent(getContext(), SelectUniversityActivity.class);
                i.putExtra("activity_result", true);
                startActivityForResult(i, ApiConstants.SEND_REQUEST);
                break;
            case R.id.btn_reserve_combo:
                FoodEntity foodEntity = menuTodayAdapter.getItemSelected();
                if (sessionManager.getUserToken() != null) {
                    if (foodEntity != null) {
                        if (foodEntity.getStock() != 0) {
                            showQuantityDialog(foodEntity);
                        } else {
                            setMessageError(getString(R.string.no_stock));
                        }
                    } else {
                        setMessageError(getString(R.string.please_choose_a_menu));
                    }
                } else {
                    setMessageError(getString(R.string.please_login_to_reserve));
                }
                break;
            case R.id.iv_arrow_left:
                position--;
                rvSelectMenu.scrollToPosition(position);
                showArrow();
                break;
            case R.id.iv_arrow_right:
                position++;
                rvSelectMenu.scrollToPosition(position);
                showArrow();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ApiConstants.SEND_REQUEST) {
            if (resultCode == ApiConstants.MENU_RESULT) {
                tvSelectPoint.setText(sessionManager.getPlace().getName());
            }
        }
    }

}
