package com.cerezaconsulting.altoke.presentation.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.data.entities.UserEntity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.presentation.fragments.ContactUsFragment;
import com.cerezaconsulting.altoke.presentation.fragments.MenuTodayFragment;
import com.cerezaconsulting.altoke.presentation.fragments.MenuWeekFragment;
import com.cerezaconsulting.altoke.presentation.presenters.MenuTodayPresenter;
import com.cerezaconsulting.altoke.presentation.presenters.MenuWeekPresenter;
import com.cerezaconsulting.altoke.presentation.utils.ActivityUtils;
import com.cerezaconsulting.altoke.presentation.utils.CircleTransform;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miguel on 16/02/17.
 */

public class MainActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.btn_menu_today)
    Button btnMenuToday;
    @BindView(R.id.btn_menu_week)
    Button btnMenuWeek;
    @BindView(R.id.body)
    FrameLayout body;
    @BindView(R.id.body2)
    FrameLayout body2;
    private ActionBarDrawerToggle mDrawerToggle;
    private MenuTodayFragment menuTodayFragment;
    private MenuWeekFragment menuWeekFragment;
    private TapButton tapButton = TapButton.TODAY;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setTitle("");
        ab.setDefaultDisplayHomeAsUpEnabled(true);
        setupDrawerContent(navigationView);

        sessionManager = new SessionManager(getApplicationContext());

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                    /* host Activity */
                drawerLayout,                    /* DrawerLayout object */
                toolbar,
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        mDrawerToggle.syncState();
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        menuTodayFragment = (MenuTodayFragment) getSupportFragmentManager().findFragmentById(R.id.body);
        if (menuTodayFragment == null) {
            menuTodayFragment = MenuTodayFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), menuTodayFragment, R.id.body);
        }
        new MenuTodayPresenter(menuTodayFragment,getApplicationContext());

        menuWeekFragment = (MenuWeekFragment) getSupportFragmentManager().findFragmentById(R.id.body2);
        if(menuWeekFragment == null){
            menuWeekFragment = MenuWeekFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),menuWeekFragment,R.id.body2);
        }

        new MenuWeekPresenter(menuWeekFragment,getApplicationContext());

        onClick(btnMenuToday);

        View header = navigationView.getHeaderView(0);
        LinearLayout headerLayout = (LinearLayout) header.findViewById(R.id.ll_header);
        ImageView headerImage = (ImageView) header.findViewById(R.id.iv_image_header);
        TextView headerText = (TextView) header.findViewById(R.id.tv_text_header);
        UserEntity userEntity = sessionManager.getUserEntity();
        if(userEntity!=null){
            headerText.setText(userEntity.getFullName());
            if(userEntity.getPhoto()!=null){
                Glide.with(this).load(userEntity.getPhoto()).bitmapTransform(new CircleTransform(getApplicationContext())).into(headerImage);
            }
            else{
                headerImage.setImageResource(R.drawable.user_big_logo);
            }
        }
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLoginOrAccount();
                drawerLayout.closeDrawers();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_update, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_update:
                switch (tapButton){
                    case TODAY:
                        if(menuTodayFragment!=null) {
                            menuTodayFragment.refresh();
                        }
                        break;
                    case WEEK:
                        if(menuWeekFragment!=null) {
                            menuWeekFragment.refresh();
                        }
                        break;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setLoginOrAccount(){
        if(sessionManager.isLogin()){
            next(this,null,AccountActivity.class,false);
        }
        else{
            next(this,null,LoginActivity.class,false);
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position

        switch (menuItem.getItemId()) {
            case R.id.action_my_account:
                setLoginOrAccount();
                break;
            case R.id.action_university:
                next(this,null,SelectUniversityActivity.class,false);
                break;
            case R.id.action_my_orders:
                if(sessionManager.isLogin()) {
                    next(this, null, MyOrdersActivity.class, false);
                }
                else{
                    showMessageError("Por favor inicie sesión para poder ver mis pedidios");
                }
                break;
            case R.id.action_help:
                next(this,null, ContactUsActivity.class,false);
                break;
            case R.id.action_payment:
                next(this,null,PaymentActivity.class,false);
                break;
            default:
                break;

        }

        // Highlight the selected item, update the title, and close the drawer
        //menuItem.setChecked(true);
        //setTitle(menuItem.getTitle());
        drawerLayout.closeDrawers();
        //this.finish();
    }

    private void checkButton(TapButton tapButton) {
        switch (tapButton) {
            case TODAY:
                btnMenuToday.setBackground(getResources().getDrawable(R.drawable.button_select));
                btnMenuToday.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                btnMenuWeek.setBackground(getResources().getDrawable(R.drawable.button_unselect));
                btnMenuWeek.setTextColor(getResources().getColor(R.color.white));
                setFragmentVisibility(tapButton);
                break;
            case WEEK:
                btnMenuToday.setBackground(getResources().getDrawable(R.drawable.button_unselect));
                btnMenuToday.setTextColor(getResources().getColor(R.color.white));
                btnMenuWeek.setBackground(getResources().getDrawable(R.drawable.button_select));
                btnMenuWeek.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                setFragmentVisibility(tapButton);
                break;
        }
    }

    private void setFragmentVisibility(TapButton tapButton) {
        switch (tapButton) {
            case TODAY:
                body.setVisibility(View.VISIBLE);
                body2.setVisibility(View.GONE);
                break;
            case WEEK:
                body.setVisibility(View.GONE);
                body2.setVisibility(View.VISIBLE);
                break;
        }
    }


    @OnClick({R.id.btn_menu_today, R.id.btn_menu_week})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_menu_today:
                tapButton = TapButton.TODAY;
                checkButton(tapButton);
                break;
            case R.id.btn_menu_week:
                tapButton = TapButton.WEEK;
                checkButton(tapButton);
                break;
        }
    }

    private enum TapButton {
        TODAY,
        WEEK
    }

}

