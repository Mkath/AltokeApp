package com.cerezaconsulting.altoke.presentation.services;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;


import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ServiceFactory;
import com.cerezaconsulting.altoke.data.repositories.remote.request.UserRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Belal on 5/27/2016.
 */


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
        if (token != null) {
            SessionManager authLocalData = new SessionManager(getApplicationContext());
            authLocalData.regID(token);


            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            // get IMEI
            String imei = tm.getDeviceId();

            if (authLocalData.isLogin()) {
                final UserRequest userRequest =
                        ServiceFactory.createService(UserRequest.class);

                Call<Void> call = userRequest.updateFCM("Token " +authLocalData.getUserToken(),
                        token, imei);

                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {



                        } else {


                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {


                    }

                });
            }


        }




    }
}