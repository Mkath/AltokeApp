package com.cerezaconsulting.altoke.presentation.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.data.entities.ReservationEntity;
import com.cerezaconsulting.altoke.presentation.utils.DayUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 19/04/17.
 */

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {

    private ArrayList<ReservationEntity> list;
    private ArrayList<Boolean> status;
    private Context context;

    public MyOrdersAdapter(ArrayList<ReservationEntity> list,Context context) {
        this.context = context;
        this.list = list;
        setStatus();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_cardview_combo, parent, false);
        return new ViewHolder(root);
    }

    private void setStatus(){
        status = new ArrayList<>();
        for (int l = 0; l < list.size(); l++) {
            status.add(false);
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ReservationEntity reservationEntity = list.get(position);
        holder.tvMenuDate.setText(reservationEntity.getDay_reservation()!=null ?
                DayUtils.getCalendarString(reservationEntity.getDay_reservation()) : "");
        holder.tvTitleCombo.setText(reservationEntity.getName_lunch());
        holder.tvMenuBeverage.setText(reservationEntity.getDrinks_name());
        holder.tvMenuEntry.setText(reservationEntity.getEntry_name());
        holder.tvMenuDish.setText(reservationEntity.getMaindish_name());
        setColorChange(holder,status.get(position));
        holder.frBackgroundTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status.set(holder.getAdapterPosition(),!status.get(holder.getAdapterPosition()));
                notifyItemChanged(holder.getAdapterPosition());
            }
        });
    }

    private void setColorChange(ViewHolder holder,boolean status){
        holder.tvMenuDate.setTextColor(status?context.getResources().getColor(R.color.white):context.getResources().getColor(R.color.colorAccent));
        holder.tvTitleCombo.setTextColor(status?context.getResources().getColor(R.color.white):context.getResources().getColor(R.color.colorAccent));
        holder.frBackgroundTitle.setBackgroundColor(status?context.getResources().getColor(R.color.colorAccent):context.getResources().getColor(R.color.white));
        holder.llDescriptionMenu.setVisibility(status?View.VISIBLE:View.GONE);
    }

    public void setItems(ArrayList<ReservationEntity> list){
        this.list = list;
        setStatus();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_menu_date)
        TextView tvMenuDate;
        @BindView(R.id.tv_title_combo)
        TextView tvTitleCombo;
        @BindView(R.id.fr_background_title)
        FrameLayout frBackgroundTitle;
        @BindView(R.id.tv_menu_entry)
        TextView tvMenuEntry;
        @BindView(R.id.tv_menu_dish)
        TextView tvMenuDish;
        @BindView(R.id.tv_menu_beverage)
        TextView tvMenuBeverage;
        @BindView(R.id.ll_description_menu)
        LinearLayout llDescriptionMenu;
        @BindView(R.id.fr_item_menu)
        LinearLayout frItemMenu;
        @BindView(R.id.card_view)
        CardView cardView;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
