package com.cerezaconsulting.altoke.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.core.BaseFragment;
import com.cerezaconsulting.altoke.core.ScrollChildSwipeRefreshLayout;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.presentation.activities.MainActivity;
import com.cerezaconsulting.altoke.presentation.activities.SelectUniversityActivity;
import com.cerezaconsulting.altoke.presentation.adapters.SelectUniversityAdapter;
import com.cerezaconsulting.altoke.presentation.contracts.SelectUniversityContract;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miguel on 16/02/17.
 */

public class SelectUniversityFragment extends BaseFragment implements SelectUniversityContract.View {


    @BindView(R.id.rv_select_university)
    RecyclerView rvSelectUniversity;
    @BindView(R.id.btn_get_in)
    Button btnGetIn;
    @BindView(R.id.refresh_layout)
    ScrollChildSwipeRefreshLayout refreshLayout;
    private SelectUniversityContract.Presenter presenter;
    private SessionManager sessionManager;
    private SelectUniversityAdapter selectUniversityAdapter;
    private GridLayoutManager layoutManager;
    private boolean activityResultStatus;

    public static SelectUniversityFragment newInstance(Bundle bundle) {
        SelectUniversityFragment fragment = new SelectUniversityFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityResultStatus=getArguments().getBoolean("activity_result",false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_select_university, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sessionManager = new SessionManager(getContext());
        refreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.white),
                ContextCompat.getColor(getActivity(), R.color.colorPrimary)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        refreshLayout.setScrollUpChild(rvSelectUniversity);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.start();
            }
        });
        layoutManager = new GridLayoutManager(getContext(), 2);
        rvSelectUniversity.setLayoutManager(layoutManager);

        selectUniversityAdapter = new SelectUniversityAdapter(new ArrayList<PlacesEntity>(), getContext());
        rvSelectUniversity.setAdapter(selectUniversityAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @OnClick(R.id.btn_get_in)
    public void onClick() {
        PlacesEntity universityEntity = selectUniversityAdapter.getItemSelected();
        if (universityEntity != null) {
            sessionManager.selectPlace(universityEntity);
            if(activityResultStatus){
                ((SelectUniversityActivity)getActivity()).responseRequest();
            }
            else {
                next(getActivity(), null, MainActivity.class, true);
            }
        }
        else{
            setMessageError("Por favor seleccione una universidad");
        }
    }

    @Override
    public void showPlaces(ArrayList<PlacesEntity> list) {
        if(selectUniversityAdapter!=null){
            if(sessionManager.getPlace()!=null){
                selectUniversityAdapter.setItems(list,sessionManager.getPlace());
            }
            else{
                selectUniversityAdapter.setItems(list);
            }
        }
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
        // Make sure setRefreshing() is called after the layout is done with everything else.
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(active);
            }
        });
    }

    @Override
    public void setMessageError(String error) {
        ((BaseActivity) getActivity()).showMessageError(error);
    }

    @Override
    public void setDialogMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(SelectUniversityContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
