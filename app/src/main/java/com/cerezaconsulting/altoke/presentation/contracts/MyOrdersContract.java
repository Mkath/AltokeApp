package com.cerezaconsulting.altoke.presentation.contracts;

import com.cerezaconsulting.altoke.core.BasePresenter;
import com.cerezaconsulting.altoke.core.BaseView;
import com.cerezaconsulting.altoke.data.entities.ReservationEntity;

import java.util.ArrayList;

/**
 * Created by miguel on 19/04/17.
 */

public interface MyOrdersContract {
    interface View extends BaseView<Presenter>{
        void getMyOrders(ArrayList<ReservationEntity> list);
    }
    interface Presenter extends BasePresenter{

    }
}
