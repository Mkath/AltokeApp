package com.cerezaconsulting.altoke.presentation.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;

import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.data.repositories.remote.ServiceFactory;
import com.cerezaconsulting.altoke.data.repositories.remote.request.UserRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 28/04/17.
 */

public class RegisterFCMActivity extends BaseActivity {
    private Activity activity = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle("Registrando dispositivo...");

        String token = FirebaseInstanceId.getInstance().getToken();

        if (token != null) {
            final SessionManager authLocalData = new SessionManager(getApplicationContext());
            authLocalData.regID(token);


            //TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            // get IMEI
            String imei = getUniqueID();

            if (authLocalData.isLogin()) {
                final UserRequest userRequest =
                        ServiceFactory.createService(UserRequest.class);

                Call<Void> call = userRequest.sendFCM("Token " + authLocalData.getUserToken(),
                        token, imei, "android");
                progressDialog.show();
                call.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        progressDialog.dismiss();
                        if (response.isSuccessful()) {
                            nextActivityNewTask(activity,null,MainActivity.class,true);
                            FirebaseMessaging.getInstance().subscribeToTopic("Briana");
                            finish();
                        } else {
                            showMessageError("Ocurrió un error, inténtelo nuevamente");
                            authLocalData.closeSession();
                            finish();

                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        progressDialog.dismiss();
                        showMessageError("Imposible conectar, revise su conexión");
                        authLocalData.closeSession();
                        finish();

                    }

                });
            }


        }


    }

    public String getUniqueID(){
        String myAndroidDeviceId = "";
        TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null){
            myAndroidDeviceId = mTelephony.getDeviceId();
        }else{
            myAndroidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return myAndroidDeviceId;
    }
}
