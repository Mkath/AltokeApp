package com.cerezaconsulting.altoke.presentation.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cerezaconsulting.altoke.BuildConfig;
import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.core.BaseFragment;
import com.cerezaconsulting.altoke.data.entities.UserEntity;
import com.cerezaconsulting.altoke.data.entities.enums.TypeLoginEnum;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.presentation.activities.MainActivity;
import com.cerezaconsulting.altoke.presentation.activities.RegisterFCMActivity;
import com.cerezaconsulting.altoke.presentation.contracts.LoginContract;
import com.cerezaconsulting.altoke.presentation.utils.ProgressDialogCustom;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miguel on 22/02/17.
 */

public class LoginFragment extends BaseFragment implements LoginContract.View,
        GoogleApiClient.OnConnectionFailedListener{


    @BindView(R.id.btn_facebook)
    LinearLayout btnFacebook;
    @BindView(R.id.btn_google)
    LinearLayout btnGoogle;

    private static final int RC_SIGN_IN = 007;
    private CallbackManager callbackManager;
    private AccessToken accessToken;
    private GoogleApiClient mGoogleApiClient;
    private SessionManager mSessionManager;
    private String photo_gmail;
    private ProgressDialogCustom mProgressDialogCustom;
    private LoginContract.Presenter mPresenter;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());

        String google_token = BuildConfig.GOOGLE_API_KEY;
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(google_token)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                Log.e("fb token",accessToken.toString());
                String access_token_facebook = loginResult.getAccessToken().getToken();
                if (access_token_facebook != null && !access_token_facebook.isEmpty()) {
                    mPresenter.loginFacebook(access_token_facebook);
                } else {
                    setMessageError("Algo sucedió mal al intentar loguearse");
                }

            }

            @Override
            public void onCancel() {
                setMessageError("El login a facebook se a cancelado, intente más tarde");
            }

            @Override
            public void onError(FacebookException error) {
                setMessageError("Error al intentar loguearse");
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Ingresando...");
    }

    @OnClick({R.id.btn_facebook, R.id.btn_google})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_facebook:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email","user_birthday"));
                break;
            case R.id.btn_google:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }

    @Override
    public void loginSuccessfully(final UserEntity userEntity, TypeLoginEnum typeLogin) {
        switch (typeLogin){
            case FACEBOOK:
                GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        response.getJSONObject();
                        try {
                            String picture = null;
                            if (!object.isNull("picture")) {
                                JSONObject data = object.getJSONObject("picture");
                                JSONObject url = data.getJSONObject("data");
                                picture = (String) url.get("url");
                                userEntity.setPhoto(picture);
                                mSessionManager.setUserEntity(userEntity);
                                successLogin();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
                break;
            case GMAIL:
                userEntity.setPhoto(photo_gmail);
                mSessionManager.setUserEntity(userEntity);
                successLogin();
                break;
        }
    }

    private void successLogin(){
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Inicio de Sesión exitoso")
                .setPositiveButton("Aceptar",null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        newFlag(getActivity(),null, RegisterFCMActivity.class);
                    }
                }).create();
        alertDialog.show();
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (getView() == null) {
            return;
        }
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            int statusCode = result.getStatus().getStatusCode();
            Log.e("data gmail",result.isSuccess()+" - "+statusCode);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                // Get account information
                if(acct!=null && acct.getPhotoUrl()!=null) {
                    photo_gmail = acct.getPhotoUrl().getPath();
                }
                Log.e("GMAIL",acct.getDisplayName()+",, "+acct.getEmail()+" ... "+acct.getIdToken()+" "+acct.getPhotoUrl().getPath());
                mPresenter.loginGmail(acct.getIdToken());
            }
        }
    }

    @Override
    public void setMessageError(String error) {
        ((BaseActivity) getActivity()).showMessageError(error);
    }

    @Override
    public void setDialogMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
