package com.cerezaconsulting.altoke.presentation.contracts;

import com.cerezaconsulting.altoke.core.BasePresenter;
import com.cerezaconsulting.altoke.core.BaseView;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;

import java.util.ArrayList;

/**
 * Created by miguel on 10/04/17.
 */

public interface SelectUniversityContract {
    interface View extends BaseView<Presenter>{
        void showPlaces(ArrayList<PlacesEntity> list);
    }
    interface Presenter extends BasePresenter{
        void loadPlaces();
        void loadMorePlaces();
    }
}
