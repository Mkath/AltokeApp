package com.cerezaconsulting.altoke.presentation.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseActivity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;

/**
 * Created by miguel on 15/02/17.
 */

public class LoaderActivity extends BaseActivity {

    private SessionManager sessionManager;
    private static final int REQUEST_PERMISSIONS_STATE_PHONE = 55;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);
        sessionManager = new SessionManager(getApplicationContext());
        checkStatePhone();
    }

    private void selectUniversity(){
        if(sessionManager.getPlace()!=null){
            next(this,null,MainActivity.class,true);
        }
        else{
            next(this,null,SelectUniversityActivity.class,true);
        }
    }

    private void checkStatePhone() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED) {
            selectUniversity();
        } else {

            requestPermissions();
        }
    }

    private void requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_PHONE_STATE)) {
            CoordinatorLayout container = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
            Snackbar.make(container, "Sin el permiso para leer el estado de tu teléfono no podrás " +
                    "ingresar ni registrate en Altoke", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            ActivityCompat.requestPermissions(LoaderActivity.this,
                                    new String[]{Manifest.permission.READ_PHONE_STATE},
                                    REQUEST_PERMISSIONS_STATE_PHONE);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    REQUEST_PERMISSIONS_STATE_PHONE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_STATE_PHONE) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectUniversity();
            } else {
                CoordinatorLayout container = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
                Snackbar.make(container, "Sin el permiso, no puedo realizar la" +
                        "ingresar a la aplicación", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
