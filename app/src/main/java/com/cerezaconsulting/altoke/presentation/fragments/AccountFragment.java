package com.cerezaconsulting.altoke.presentation.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cerezaconsulting.altoke.R;
import com.cerezaconsulting.altoke.core.BaseFragment;
import com.cerezaconsulting.altoke.data.entities.PlacesEntity;
import com.cerezaconsulting.altoke.data.entities.UserEntity;
import com.cerezaconsulting.altoke.data.repositories.local.SessionManager;
import com.cerezaconsulting.altoke.presentation.activities.MainActivity;
import com.cerezaconsulting.altoke.presentation.utils.CircleTransform;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by miguel on 23/02/17.
 */

public class AccountFragment extends BaseFragment {


    @BindView(R.id.iv_user)
    ImageView ivUser;
    @BindView(R.id.tv_user_university)
    TextView tvUserUniversity;
    @BindView(R.id.card_view)
    CardView cardView;
    @BindView(R.id.btn_close_session)
    Button btnCloseSession;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_user_email)
    TextView tvUserEmail;

    private SessionManager sessionManager;

    public static AccountFragment newInstance() {
        return new AccountFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sessionManager = new SessionManager(getContext());
        UserEntity userEntity = sessionManager.getUserEntity();
        PlacesEntity placesEntity = sessionManager.getPlace();
        if(userEntity.getPhoto()!=null){
            Glide.with(this).load(userEntity.getPhoto()).bitmapTransform(new CircleTransform(getContext())).into(ivUser);
        }
        tvUserName.setText(userEntity.getFullName());
        tvUserEmail.setText(userEntity.getEmail());
        tvUserUniversity.setText(placesEntity.getName());
    }

    @OnClick(R.id.btn_close_session)
    public void onClick() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Cerrar Sesión")
                .setMessage("¿Está seguro que desea cerrar sesión?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sessionManager.closeSession();
                        closeSession();
                    }
                })
                .setNegativeButton("Cancelar", null)
                .create();
        alertDialog.show();
    }

    private void closeSession() {
        AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                .setTitle("Sesión terminada")
                .setPositiveButton("Aceptar", null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        newFlag(getActivity(), null, MainActivity.class);
                    }
                }).create();
        alertDialog.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
