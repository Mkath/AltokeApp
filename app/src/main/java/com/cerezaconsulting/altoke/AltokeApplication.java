package com.cerezaconsulting.altoke;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AltokeApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/gotham_light.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
